using MessageSystem;
using NaconTest.RuntimePackages.Enums;
using NaconTest.RuntimePackages.Tile;
using System.Collections.Generic;
using UnityEngine;

namespace NaconTest.RuntimePackages.Player {
    public class PathComposer : MonoBehaviour {
        [Header("References")]
        [SerializeField] private TilesMapGenerationListener listener = default;

        private TileCell playerCell = default;
        private TileCell[,] map = default;

        private void Awake() {
            if (listener == null)
                return;

            listener.OnCompleteGeneration += RegisterMap;
        }
        private void OnDestroy() {
            if (listener == null)
                return;

            listener.OnCompleteGeneration -= RegisterMap;
        }

        private void RegisterMap(MessageClass message) {
            if (!MessageClass.TryCastTo(message, out BoardGenerationMessage expectedMessage))
                return;

            map = expectedMessage.Map;
        }
        public void PlayerStartingCell(TileCell cell) {
            if (cell == null)
                return;

            playerCell = cell;
            cell.IsVisited = true;
        }

        public bool CanMoveTo(DirectionTypes direction) {
            return playerCell.Entrances.HasFlag(direction);
        }

        public List<Vector3> Path(DirectionTypes direction) {
            List<Vector3> fullPath = new List<Vector3>();

            // Get direction neighbour of player cell
            // Is neighbour a tunnel?

            // Yes -> Try get "innested" neighbour until last is not a tunnel
            // For each neighbour get path and add all points to fullPath (technically the tunnels' paths should be reordered respect which entrance we are considering)

            // No -> Sum paths of player cell and neighbour

            return fullPath;
        }
    }
}