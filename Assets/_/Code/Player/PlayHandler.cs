using MessageSystem;
using NaconTest.RuntimePackages.Enums;
using NaconTest.RuntimePackages.Tile;
using UnityEngine;

namespace NaconTest.RuntimePackages.Player {
    public class PlayHandler : MonoBehaviour {
        [Header("Settings")]
        [SerializeField] private TileBehaviourTypes requiredBehaviour = default;

        [Header("References")]
        [SerializeField] private TilesMapGenerationListener listener = default;
        [Space]
        [SerializeField] private PathComposer pathComposer = default;

        [Header("Prefabs")]
        [SerializeField] private GameObject playerPrefabs = default;

        private void Awake() {
            if (listener == null)
                return;

            listener.OnCompleteGeneration += InitializePlayer;
        }
        private void OnDestroy() {
            if (listener == null)
                return;

            listener.OnCompleteGeneration -= InitializePlayer;
        }

        private void InitializePlayer(MessageClass message) {
            if (!MessageClass.TryCastTo(message, out BoardGenerationMessage expectedMessage))
                return;

            PlayerStartingCell(expectedMessage.Map);
        }
        private void PlayerStartingCell(TileCell[,] map) {
            bool startingPointFound = default;
            TileCell selectedCell = default;

            while (!startingPointFound) {
                int i = Random.Range(0, map.GetLength(0));
                int j = Random.Range(0, map.GetLength(1));
                selectedCell = map[i, j];

                if (selectedCell.Behaviour == requiredBehaviour) {
                    startingPointFound = true;
                }
            }

            GameObject player = Instantiate(playerPrefabs, selectedCell.TileUnityObject.GetDefaultPoint(), Quaternion.identity);
            if (player.TryGetComponent(out PlayerMovementController movementController)) {
                movementController.AssignPathComposer(pathComposer);
                pathComposer.PlayerStartingCell(selectedCell);
            }
        }
    }
}