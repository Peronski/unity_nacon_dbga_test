using System;

namespace NaconTest.RuntimePackages.Enums {
    [Serializable]
    [Flags]
    public enum DirectionTypes {
        None = 0,
        Up = 1,
        Down = 2,
        Left = 4,
        Right = 8
    }

    public enum TileAspectTypes {
        Default,
        Tunnel,
        Monster
    }

    public enum TileBehaviourTypes {
        Default = 0,
        Tunnel = 1,
        Acid = 2,
        Teleport = 4,
        Monster = 8
    }

    public enum GenerationTypes {
        Default = 0,
        Map,
        Maze,
        Aspect,
        Complete
    }
}