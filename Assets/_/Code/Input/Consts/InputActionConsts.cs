using System;

namespace NaconTest.RuntimePackages.Input {
    public static partial class InputActionConsts {
        public const string MOVEMENT_MAP = "Movement";
        public const string FIRE_MAP = "Fire";

        public const string UP_BUTTON = "Up Button";
        public const string DOWN_BUTTON = "Down Button";
        public const string LEFT_BUTTON = "Left Button";
        public const string RIGHT_BUTTON = "Right Button";

        public static string ConvertEnumToConst(MapNames map) {
            switch (map) {
                case MapNames.MOVEMENT: return MOVEMENT_MAP;
                case MapNames.FIRE: return FIRE_MAP;
                default: return string.Empty;
            }
        }
        public static string ConvertEnumToConst(ActionNames action) {
            switch (action) {
                case ActionNames.UP_BUTTON: return UP_BUTTON;
                case ActionNames.LEFT_BUTTON: return LEFT_BUTTON;
                case ActionNames.RIGHT_BUTTON: return RIGHT_BUTTON;
                case ActionNames.DOWN_BUTTON: return DOWN_BUTTON;
                default: return string.Empty;
            }
        }
    }

    [Serializable]
    public enum MapNames {
        MOVEMENT,
        FIRE
    }
    [Serializable]
    [Flags]
    public enum ActionNames {
        UP_BUTTON,
        LEFT_BUTTON,
        RIGHT_BUTTON,
        DOWN_BUTTON
    }
}