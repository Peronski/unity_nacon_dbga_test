using NaconTest.RuntimePackages.Enums;
using NaconTest.RuntimePackages.Input;
using System.Collections.Generic;
using UnityEngine;

namespace NaconTest.RuntimePackages.Player {
    public class PlayerMovementController : MonoBehaviour {
        [Header("References")]
        [SerializeField] private PlayerMovementActionListener listener = default;

        private bool isProcessingPath = default;
        private PathComposer pathComposer = default;

        private List<Vector3> currentPath = default;
        private void Awake() {
            listener.OnMovementInputEvent += RequestPath;
        }
        private void OnDestroy() {
            listener.OnMovementInputEvent -= RequestPath;
        }
        private void Update() {
            if (!isProcessingPath)
                return;

            Move(currentPath);
        }

        public void AssignPathComposer(PathComposer pathComposer) {
            if (pathComposer == null)
                return;

            this.pathComposer = pathComposer;
        }

        private void RequestPath(DirectionTypes direction) {
            if (isProcessingPath)
                return;

            this.MonoLog($"Direction Movement {direction}.");

            isProcessingPath = pathComposer.CanMoveTo(direction);
            currentPath = pathComposer.Path(direction);
        }

        private void Move(List<Vector3> path) {
            // Lerp between path points

            // Has reached last point?
            // Yes -> isProcessing = false;
            // No -> Go on
        }
    }
}
