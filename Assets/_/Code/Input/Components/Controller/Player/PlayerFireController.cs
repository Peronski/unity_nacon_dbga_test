using NaconTest.RuntimePackages.Enums;
using NaconTest.RuntimePackages.Input;
using UnityEngine;

namespace NaconTest.RuntimePackages.Player {
    public class PlayerFireController : MonoBehaviour {
        [Header("References")]
        [SerializeField] private PlayerFireActionListener listener = default;

        private void Awake() {
            listener.OnFireInputEvent += RequestFire;
        }
        private void OnDestroy() {
            listener.OnFireInputEvent -= RequestFire;
        }

        private void RequestFire(DirectionTypes direction) {
            this.MonoLog($"Direction Fire {direction}.");
        }
    }
}
