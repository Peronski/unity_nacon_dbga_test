using System;
using UnityEngine.InputSystem;
using Barbaragno.RuntimePackages.Input;
using NaconTest.RuntimePackages.Enums;

namespace NaconTest.RuntimePackages.Input {
    public class PlayerFireActionListener : InputActionListener {
        public event Action<DirectionTypes> OnFireInputEvent = default;

        protected override void BindInputActions() {
            InputActionSubscriptionHandler.Instance.AskSubscriptionAction(InputActionConsts.FIRE_MAP, InputActionConsts.UP_BUTTON, FireUp, ActionState.Performed);
            InputActionSubscriptionHandler.Instance.AskSubscriptionAction(InputActionConsts.FIRE_MAP, InputActionConsts.LEFT_BUTTON, FireLeft, ActionState.Performed);
            InputActionSubscriptionHandler.Instance.AskSubscriptionAction(InputActionConsts.FIRE_MAP, InputActionConsts.RIGHT_BUTTON, FireRight, ActionState.Performed);
            InputActionSubscriptionHandler.Instance.AskSubscriptionAction(InputActionConsts.FIRE_MAP, InputActionConsts.DOWN_BUTTON, FireDown, ActionState.Performed);
        }

        protected override void UnbindInputActions() {
            InputActionSubscriptionHandler.Instance.AskUnsubscriptionAction(InputActionConsts.FIRE_MAP, InputActionConsts.UP_BUTTON, FireUp, ActionState.Performed);
            InputActionSubscriptionHandler.Instance.AskUnsubscriptionAction(InputActionConsts.FIRE_MAP, InputActionConsts.LEFT_BUTTON, FireLeft, ActionState.Performed);
            InputActionSubscriptionHandler.Instance.AskUnsubscriptionAction(InputActionConsts.FIRE_MAP, InputActionConsts.RIGHT_BUTTON, FireRight, ActionState.Performed);
            InputActionSubscriptionHandler.Instance.AskUnsubscriptionAction(InputActionConsts.FIRE_MAP, InputActionConsts.DOWN_BUTTON, FireDown, ActionState.Performed);
        }

        private void FireUp(InputAction.CallbackContext context) {
            context.ReadValueAsButton();
            OnFireInputEvent?.Invoke(DirectionTypes.Up);
        }
        private void FireLeft(InputAction.CallbackContext context) {
            context.ReadValueAsButton();
            OnFireInputEvent?.Invoke(DirectionTypes.Left);
        }
        private void FireRight(InputAction.CallbackContext context) {
            context.ReadValueAsButton();
            OnFireInputEvent?.Invoke(DirectionTypes.Right);
        }
        private void FireDown(InputAction.CallbackContext context) {
            context.ReadValueAsButton();
            OnFireInputEvent?.Invoke(DirectionTypes.Down);
        }
    }
}
