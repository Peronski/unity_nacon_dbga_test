using System;
using UnityEngine.InputSystem;
using Barbaragno.RuntimePackages.Input;
using NaconTest.RuntimePackages.Enums;

namespace NaconTest.RuntimePackages.Input {
    public class PlayerMovementActionListener : InputActionListener {
        public event Action<DirectionTypes> OnMovementInputEvent = default;

        protected override void BindInputActions() {
            InputActionSubscriptionHandler.Instance.AskSubscriptionAction(InputActionConsts.MOVEMENT_MAP, InputActionConsts.UP_BUTTON, MoveUp, ActionState.Performed);
            InputActionSubscriptionHandler.Instance.AskSubscriptionAction(InputActionConsts.MOVEMENT_MAP, InputActionConsts.LEFT_BUTTON, MoveLeft, ActionState.Performed);
            InputActionSubscriptionHandler.Instance.AskSubscriptionAction(InputActionConsts.MOVEMENT_MAP, InputActionConsts.RIGHT_BUTTON, MoveRight, ActionState.Performed);
            InputActionSubscriptionHandler.Instance.AskSubscriptionAction(InputActionConsts.MOVEMENT_MAP, InputActionConsts.DOWN_BUTTON, MoveDown, ActionState.Performed);
        }

        protected override void UnbindInputActions() {
            InputActionSubscriptionHandler.Instance.AskUnsubscriptionAction(InputActionConsts.MOVEMENT_MAP, InputActionConsts.UP_BUTTON, MoveUp, ActionState.Performed);
            InputActionSubscriptionHandler.Instance.AskUnsubscriptionAction(InputActionConsts.MOVEMENT_MAP, InputActionConsts.LEFT_BUTTON, MoveLeft, ActionState.Performed);
            InputActionSubscriptionHandler.Instance.AskUnsubscriptionAction(InputActionConsts.MOVEMENT_MAP, InputActionConsts.RIGHT_BUTTON, MoveRight, ActionState.Performed);
            InputActionSubscriptionHandler.Instance.AskUnsubscriptionAction(InputActionConsts.MOVEMENT_MAP, InputActionConsts.DOWN_BUTTON, MoveDown, ActionState.Performed);
        }

        private void MoveUp(InputAction.CallbackContext context) {
            context.ReadValueAsButton();
            OnMovementInputEvent?.Invoke(DirectionTypes.Up);
        }
        private void MoveLeft(InputAction.CallbackContext context) {
            context.ReadValueAsButton();
            OnMovementInputEvent?.Invoke(DirectionTypes.Left);
        }
        private void MoveRight(InputAction.CallbackContext context) {
            context.ReadValueAsButton();
            OnMovementInputEvent?.Invoke(DirectionTypes.Right);
        }
        private void MoveDown(InputAction.CallbackContext context) {
            context.ReadValueAsButton();
            OnMovementInputEvent?.Invoke(DirectionTypes.Down);
        }
    }
}
