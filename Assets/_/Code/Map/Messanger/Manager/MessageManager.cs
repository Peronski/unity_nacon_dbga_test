using NaconTest.RuntimePackages.Tile;

namespace MessageSystem {
    public static partial class MessageManager {
        public static MessageChannel<GenerationPhaseTypes> BoardGeneration = new MessageChannel<GenerationPhaseTypes>();
    }

    public enum GenerationPhaseTypes {
        Default,
        Map,
        Maze,
        Aspect,
        Complete
    }

    public class BoardGenerationMessage : MessageClass {
        public TileCell[,] Map { get; set; }
    }
}
