using MessageSystem;
using System;
using UnityEngine;

public class TilesMapGenerationListener : MonoBehaviour {
    public Action<MessageClass> OnDefaultGeneration = default;
    public Action<MessageClass> OnMapGeneration = default;
    public Action<MessageClass> OnMazeGeneration = default;
    public Action<MessageClass> OnAspectGeneration = default;
    public Action<MessageClass> OnCompleteGeneration = default;

    private void Awake() {
        MessageManager.BoardGeneration.AttachListener(GenerationPhaseTypes.Default, Default);
        MessageManager.BoardGeneration.AttachListener(GenerationPhaseTypes.Map, MapGeneration);
        MessageManager.BoardGeneration.AttachListener(GenerationPhaseTypes.Maze, MazeGeneration);
        MessageManager.BoardGeneration.AttachListener(GenerationPhaseTypes.Aspect, AspectGeneration);
        MessageManager.BoardGeneration.AttachListener(GenerationPhaseTypes.Complete, Complete);
    }
    private void OnDestroy() {
        MessageManager.BoardGeneration.DetachListener(GenerationPhaseTypes.Default, Default);
        MessageManager.BoardGeneration.DetachListener(GenerationPhaseTypes.Map, MapGeneration);
        MessageManager.BoardGeneration.DetachListener(GenerationPhaseTypes.Maze, MazeGeneration);
        MessageManager.BoardGeneration.DetachListener(GenerationPhaseTypes.Aspect, AspectGeneration);
        MessageManager.BoardGeneration.DetachListener(GenerationPhaseTypes.Complete, Complete);
    }

    private void Default(GenerationPhaseTypes type, MessageClass message) {
        OnDefaultGeneration?.Invoke(message);
    }
    private void MapGeneration(GenerationPhaseTypes type, MessageClass message) {
        OnMapGeneration?.Invoke(message);
    }
    private void MazeGeneration(GenerationPhaseTypes type, MessageClass message) {
        OnMazeGeneration?.Invoke(message);  
    }
    private void AspectGeneration(GenerationPhaseTypes type, MessageClass message) {
        OnAspectGeneration?.Invoke(message);
    }
    private void Complete(GenerationPhaseTypes type, MessageClass message) {
        OnCompleteGeneration?.Invoke(message);
    }
}
