using Barbaragno.RuntimePackages.SerializableUtility.Dictionary;
using MessageSystem;
using NaconTest.RuntimePackages.Enums;
using NaconTest.RuntimePackages.Tile;
using System;
using System.Collections;
using UnityEngine;

namespace NaconTest.RuntimePackages.Map {
    public class TilesMapGenerator : MonoBehaviour, IEnumeratorRunner<TileCell[,]> {
        [Header("Settings")]
        [SerializeField] private GenerationTypes startingGeneration = default;

        [Header("Data")]
        [SerializeField] private SerializableDictionary<GenerationTypes, GenerationLogicSO<TileCell[,]>> generationLogics = default;

        private GenerationTypes currentGeneration = default;
        private GenerationLogicSO<TileCell[,]> currentLogic = default;

        public TileCell[,] Result { get; private set; }

        public event Action<TileCell[,]> OnGameBoardGenerationComplete = default;

        private void Awake() {
            StartGeneration(startingGeneration);
        }

        private void Generate(GenerationTypes generation) {
            if (currentGeneration == GenerationTypes.Complete) {
                EndGeneration();
                return;
            }

            if (!generationLogics.ContainsKey(generation)) {
                return;
            }

            currentLogic = generationLogics[generation];
            currentLogic.InjectRunner(this);
            currentLogic.StartGeneration();
        }

        private void StartGeneration(GenerationTypes starting = GenerationTypes.Default) {
            currentGeneration = starting;
            Generate(currentGeneration);
        }
        private void NextGeneration() {
            currentGeneration++;
            Generate(currentGeneration);
        }
        private void EndGeneration() {
            OnGameBoardGenerationComplete?.Invoke(Result);
            MessageManager.BoardGeneration.SendMessage(GenerationPhaseTypes.Complete, new BoardGenerationMessage() { Map = Result });
        }

        // IENUMERATOR RUNNER IMPLEMENTATION
        public Coroutine Run(Func<IEnumerator> coroutine) {
            if (coroutine == null) {
                this.MonoLog($"Func<IEnumerator> {coroutine.Method.Name} is invalid.", LogType.Warning);
                return null;
            }

            this.MonoLog($"Coroutine {coroutine.Method.Name} Generation - [Status.Running].");
            return StartCoroutine(coroutine());
        }
        public void Stop(Coroutine coroutine) {
            if (coroutine == null) {
                this.MonoLog($"Coroutine {coroutine} is invalid.", LogType.Warning);
                return;
            }

            this.MonoLog($"Coroutine {coroutine} Generation - [Status.Stopping].");
            StopCoroutine(coroutine);
        }
        public void OnStart() {
            this.MonoLog("Generation - [Status.Start].");
        }
        public void OnComplete(TileCell[,] map) {
            this.MonoLog("Generation - [Status.Complete].");
            Result = map;
            NextGeneration();
        }
    }
}