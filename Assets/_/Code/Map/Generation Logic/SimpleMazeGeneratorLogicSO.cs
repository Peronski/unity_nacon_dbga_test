using MessageSystem;
using NaconTest.RuntimePackages.Enums;
using NaconTest.RuntimePackages.Tile;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace NaconTest.Data.Tile {
    [CreateAssetMenu(fileName = "New Maze Generation Logic", menuName = "NaconTest/Scriptable Objects/Generation Logic/Simple Maze Generation Logic")]
    public class SimpleMazeGeneratorLogicSO : GenerationLogicSO<TileCell[,]> {
        [Header("Settings")]
        [SerializeField] private int startingCell_X = default;
        [SerializeField] private int startingCell_Y = default;
        [Space]
        [SerializeField] private float waitTime = default;

        [Header("Debug")]
        [SerializeField] private bool useLog = default;

        private int matrix_X = default;
        private int matrix_Y = default;
        private int visitedCells = default;

        private Stack<TileCell> stack = new Stack<TileCell>();

        private IEnumeratorRunner<TileCell[,]> runner = default;

        private Coroutine CO_Generation = default;

        private WaitForSeconds waitForSeconds = default;

        private void OnValidate() {
            Clean();
        }
        private void Setup() {
            result = runner.Result;
            matrix_X = result.GetLength(0);
            matrix_Y = result.GetLength(1);
            waitForSeconds = new WaitForSeconds(waitTime);
            stack = new Stack<TileCell>();
            StartingPoint();
        }
        private void Clean() {
            result = default;
            matrix_X = matrix_Y = 0;
            waitForSeconds = default;
            visitedCells = default;
            runner = null;
            CO_Generation = default;
            stack.Clear();
        }
        private void StartingPoint() {
            TileCell currentCell = result[startingCell_X, startingCell_Y];
            stack.Push(currentCell);
            Offset(0, 0).PathfinderHasVisited = true;
            visitedCells++;
        }
        private IEnumerator CleanVisualizers() {
            for (int i = 0; i < matrix_X; i++) {
                for (int j = 0; j < matrix_Y; j++) {
                    result[i, j].PathfinderHasVisited = false;
                }
                yield return waitForSeconds;
            }
        }
        protected override IEnumerator CO_Generate() {
            while (visitedCells < matrix_X * matrix_Y) {
                if (useLog) this.Log($"Cells visited: {visitedCells}");
                Stack<(DirectionTypes, TileCell)> neighbours = new Stack<(DirectionTypes, TileCell)>();

                if (Offset(0, 0).X > 0 && !Offset(-1, 0).PathfinderHasVisited) {
                    neighbours.Push((DirectionTypes.Up, Offset(-1, 0)));
                }
                if (Offset(0, 0).Y < matrix_Y - 1 && !Offset(0, 1).PathfinderHasVisited) {
                    neighbours.Push((DirectionTypes.Right, Offset(0, 1)));
                }
                if (Offset(0, 0).X < matrix_X - 1 && !Offset(1, 0).PathfinderHasVisited) {
                    neighbours.Push((DirectionTypes.Down, Offset(1, 0)));
                }
                if (Offset(0, 0).Y > 0 && !Offset(0, -1).PathfinderHasVisited) {
                    neighbours.Push((DirectionTypes.Left, Offset(0, -1)));
                }

                if (neighbours.Any()) {
                    (DirectionTypes, TileCell) neighbour = neighbours.ElementAt(Random.Range(0, neighbours.Count));
                    switch (neighbour.Item1) {
                        case DirectionTypes.Up:
                            Offset(0, 0).SetDirections(DirectionTypes.Up);
                            neighbour.Item2.SetDirections(DirectionTypes.Down);
                            stack.Push(neighbour.Item2);
                            break;
                        case DirectionTypes.Right:
                            Offset(0, 0).SetDirections(DirectionTypes.Right);
                            neighbour.Item2.SetDirections(DirectionTypes.Left);
                            stack.Push(neighbour.Item2);
                            break;
                        case DirectionTypes.Down:
                            Offset(0, 0).SetDirections(DirectionTypes.Down);
                            neighbour.Item2.SetDirections(DirectionTypes.Up);
                            stack.Push(neighbour.Item2);
                            break;
                        case DirectionTypes.Left:
                            Offset(0, 0).SetDirections(DirectionTypes.Left);
                            neighbour.Item2.SetDirections(DirectionTypes.Right);
                            stack.Push(neighbour.Item2);
                            break;
                    }

                    Offset(0, 0).PathfinderHasVisited = true;
                    visitedCells++;
                    yield return waitForSeconds;

                } else {
                    stack.Pop();
                    yield return waitForSeconds;
                }
            }

            yield return CleanVisualizers();

            MessageManager.BoardGeneration.SendMessage(GenerationPhaseTypes.Maze);
            GenerationComplete();
        }
        private TileCell Offset(int x, int y) {
            return result[stack.Peek().X + x, stack.Peek().Y + y];
        }

        public override void InjectRunner(IEnumeratorRunner<TileCell[,]> runner) {
            this.runner = runner;
            OnGenerationStarted += runner.OnStart;
            OnGenerationCompleted += runner.OnComplete;
        }
        public override void StartGeneration() {
            Setup();
            CO_Generation = runner.Run(CO_Generate);
            OnGenerationStarted?.Invoke();
        }
        public override void GenerationComplete() {
            runner.Stop(CO_Generation);
            OnGenerationCompleted?.Invoke(result);
            Clean();
        }
    }
}