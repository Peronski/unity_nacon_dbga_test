using MessageSystem;
using NaconTest.RuntimePackages.Tile;
using System.Collections;
using UnityEngine;

namespace NaconTest.Data.Tile {
    [CreateAssetMenu(fileName = "New Map Generation Logic", menuName = "NaconTest/Scriptable Objects/Generation Logic/Simple Map Generation Logic")]
    public class SimpleMapGeneratorLogicSO : GenerationLogicSO<TileCell[,]> {
        [Header("Settings")]
        [SerializeField] private int matrix_X = default;
        [SerializeField] private int matrix_Y = default;
        [Space]
        [SerializeField] private int tileWidth = default;
        [SerializeField] private int tileHeight = default;
        [Space]
        [SerializeField] private float waitTime = default;
        [Space]
        [SerializeField] Vector3 centerMap = default;

        [Header("Debug")]
        [SerializeField] private bool useLog = default;

        [Header("Prefabs")]
        [SerializeField] private TileUnityObject tilePrefab = default;

        private Coroutine CO_Generation = default;

        Vector3 leftAnchorMap = default;
        Vector3 startingGenerationPoint = default;

        private WaitForSeconds waitForSeconds = default;

        private IEnumeratorRunner<TileCell[,]> runner = default;

        private void OnValidate() {
            Clean();
        }
        private void Setup() {
            AssignLeftAnchorMap();
            AssignStartingGenerationPoint();
            waitForSeconds = new WaitForSeconds(waitTime);
        }
        private void Clean() {
            result = default;
            waitForSeconds = default;
            runner = null;
            CO_Generation = default;
        }
        private void AssignLeftAnchorMap() {
            float halfMapWindth = (tileWidth * matrix_Y) / 2f;
            float halfMapHeight = (tileHeight * matrix_X) / 2f;
            leftAnchorMap = centerMap + new Vector3(-halfMapWindth, 0, halfMapHeight);
            if(useLog) this.Log($"Left map anchor position: {leftAnchorMap}");
        }
        private void AssignStartingGenerationPoint() {
            float halfTileWindth = tileWidth / 2f;
            float halfTileHeight = tileHeight / 2f;
            startingGenerationPoint = leftAnchorMap + new Vector3(halfTileWindth, 0, -halfTileHeight);
            if (useLog) this.Log($"Starting point position: {startingGenerationPoint}");
        }
        protected override IEnumerator CO_Generate() {
            AssignLeftAnchorMap();
            AssignStartingGenerationPoint();

            GameObject boardMap = new GameObject("BoardMap");
            boardMap.transform.position = centerMap;

            result = new TileCell[matrix_X, matrix_Y];
            Vector3 currentPosition = startingGenerationPoint;

            for (int i = 0; i < matrix_X; i++) {
                currentPosition += new Vector3(0, 0, i * -tileHeight);
                if (useLog) this.Log($"Row {i} - SpawnPoint: {currentPosition}");
                for (int j = 0; j < matrix_Y; j++) {
                    if (useLog) this.Log($"Column {j} - SpawnPoint: {currentPosition}");
                    TileUnityObject tile = Instantiate(tilePrefab, currentPosition, Quaternion.identity);
                    tile.transform.SetParent(boardMap.transform);
                    TileCell cell = new TileCell(i, j, tile);
                    result[i, j] = cell;
                    currentPosition += new Vector3(tileWidth, 0, 0);
                }
                currentPosition = startingGenerationPoint;
                yield return waitForSeconds;
            }

            MessageManager.BoardGeneration.SendMessage(GenerationPhaseTypes.Map);
            GenerationComplete();
        }
        public override void InjectRunner(IEnumeratorRunner<TileCell[,]> runner) {
            if(runner == null) {
                this.Log("Invalid Runner", LogType.Error);
                return;
            }

            this.runner = runner;
            OnGenerationStarted += runner.OnStart;
            OnGenerationCompleted += runner.OnComplete;
        }
        public override void StartGeneration() {
            Setup();
            CO_Generation = runner.Run(CO_Generate);
            OnGenerationStarted?.Invoke();
        }
        public override void GenerationComplete() {
            runner.Stop(CO_Generation);
            OnGenerationCompleted?.Invoke(result);
            Clean();
        }
    }
}
