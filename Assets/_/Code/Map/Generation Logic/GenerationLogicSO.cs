using System;
using System.Collections;
using UnityEngine;

public abstract class GenerationLogicSO<T> : ScriptableObject {
    protected T result = default;

    protected Action OnGenerationStarted = default;
    protected Action<T> OnGenerationCompleted = default;

    protected abstract IEnumerator CO_Generate();
    public abstract void InjectRunner(IEnumeratorRunner<T> runner);
    public abstract void StartGeneration();
    public abstract void GenerationComplete();
}

public interface IEnumeratorRunner<T> {
    public T Result { get; }
    public Coroutine Run(Func<IEnumerator> coroutine);
    public void Stop(Coroutine coroutine);
    public void OnStart();
    public void OnComplete(T result);
}
