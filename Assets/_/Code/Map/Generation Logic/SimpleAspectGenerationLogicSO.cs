using MessageSystem;
using NaconTest.RuntimePackages.Enums;
using NaconTest.RuntimePackages.Tile;
using System.Collections;
using UnityEngine;

namespace NaconTest.Data.Tile {
    [CreateAssetMenu(fileName = "New Aspect Generation Logic", menuName = "NaconTest/Scriptable Objects/Generation Logic/Simple Aspect Generation Logic")]
    public class SimpleAspectGenerationLogicSO : GenerationLogicSO<TileCell[,]> {
        [Header("Settings")]
        [SerializeField] private int totalTunnels = default;
        [SerializeField] private int totalTeleports = default;
        [SerializeField] private int totalAcids = default;
        [SerializeField] private int totalMonsters = default;
        [Space]
        [SerializeField] private float waitTime = default;

        [Header("Debug")]
#pragma warning disable
        [SerializeField] private bool useLog = default;
#pragma warning enable

        private int matrix_X = default;
        private int matrix_Y = default;

        private IEnumeratorRunner<TileCell[,]> runner = default;

        private Coroutine CO_Generation;

        private WaitForSeconds waitForSeconds = default;

        private void OnValidate() {
            Clean();
        }

        private void Setup() {
            result = runner.Result;
            matrix_X = result.GetLength(0);
            matrix_Y = result.GetLength(1);
            waitForSeconds = new WaitForSeconds(waitTime);
        }
        private void Clean() {
            result = default;
            matrix_X = matrix_Y = default;
            waitForSeconds = default;
            runner = null;
            CO_Generation = default;
        }

        protected override IEnumerator CO_Generate() {

            yield return ConfigureTunnels(totalTunnels);
            yield return ConfigureType(totalTeleports, TileAspectTypes.Default, TileBehaviourTypes.Teleport);
            yield return ConfigureType(totalAcids, TileAspectTypes.Default, TileBehaviourTypes.Acid);
            yield return ConfigureType(totalMonsters, TileAspectTypes.Default, TileBehaviourTypes.Monster);
            yield return ConfigureDefault();
            yield return ConfigureFog();

            MessageManager.BoardGeneration.SendMessage(GenerationPhaseTypes.Aspect);
            GenerationComplete();
        }

        private IEnumerator ConfigureDefault() {
            for (int i = 0; i < matrix_X; i++) {
                for (int j = 0; j < matrix_Y; j++) {
                    if (!result[i, j].IsConfigured) {
                        result[i, j].SetAspect(TileAspectTypes.Default);
                        result[i, j].SetBehaviour(TileBehaviourTypes.Default);
                        result[i, j].IsConfigured = true;
                    }
                    yield return waitForSeconds;
                }
            }
        }
        private IEnumerator ConfigureTunnels(int total) {
            int assigned = default;

            while (assigned < total) {
                int i = Random.Range(0,matrix_X);
                int j = Random.Range(0, matrix_Y);
                TileCell selected = result[i, j];

                if (!selected.IsConfigured) {
                    if (selected.Entrances.GetNumberFlags() == 2 && !(selected.Entrances == (DirectionTypes.Up | DirectionTypes.Down)) && !(selected.Entrances == (DirectionTypes.Left | DirectionTypes.Right))) {
                        selected.SetAspect(TileAspectTypes.Tunnel);
                        selected.SetBehaviour(TileBehaviourTypes.Tunnel);
                        selected.IsConfigured = true;
                        assigned++;
                    }
                }

                yield return waitForSeconds;
            }
        }
        private IEnumerator ConfigureType(int total, TileAspectTypes aspect, TileBehaviourTypes behaviour) {
            int assigned = default;

            while (assigned < total) {
                int i = Random.Range(0, matrix_X);
                int j = Random.Range(0, matrix_Y);
                TileCell selected = result[i, j];

                if (!selected.IsConfigured) {
                    selected.SetAspect(aspect);
                    selected.SetBehaviour(behaviour);
                    selected.IsConfigured = true;
                    assigned++;
                }

                yield return waitForSeconds;
            }
        }
        private IEnumerator ConfigureFog() {
            for (int i = 0; i < matrix_X; i++) {
                for (int j = 0; j < matrix_Y; j++) {
                    result[i, j].IsVisited = false;
                }

                yield return waitForSeconds;
            }
        }

        public override void InjectRunner(IEnumeratorRunner<TileCell[,]> runner) {
            if (runner == null) {
                this.Log("Invalid Runner", LogType.Error);
                return;
            }

            this.runner = runner;
            OnGenerationStarted += runner.OnStart;
            OnGenerationCompleted += runner.OnComplete;
        }
        public override void StartGeneration() {
            Setup();
            CO_Generation = runner.Run(CO_Generate);
            OnGenerationStarted?.Invoke();
        }
        public override void GenerationComplete() {
            runner.Stop(CO_Generation);
            OnGenerationCompleted?.Invoke(result);
            Clean();
        }
    }
}
