using NaconTest.RuntimePackages.Enums;
using NaconTest.RuntimePackages.Tile;
using System.Collections;
using UnityEngine;

namespace NaconTest.Data.Tile {
    [System.Obsolete]
    [CreateAssetMenu(fileName = "New Player Generation Logic", menuName = "NaconTest/Scriptable Objects/Generation Logic/Simple Player Generation Logic")]
    public class SimplePlayerGenerationLogicSO : GenerationLogicSO<TileCell[,]> {
        [Header("Settings")]
        [SerializeField][Range(0, 1)] private int totalPlayers = default;
        [Space]
        [SerializeField] private float time = default;

        [Header("Prefabs")]
        [SerializeField] private GameObject playerPrefabs = default;

        [Header("Debug")]
        [SerializeField] private bool useLog = default;

        private int matrix_X = default;
        private int matrix_Y = default;

        private IEnumeratorRunner<TileCell[,]> runner = default;

        private Coroutine CO_Generation;

        private WaitForSeconds waiter = default;

        private void OnValidate() {
            Clean();
        }

        private void Setup() {
            result = runner.Result;
            matrix_X = result.GetLength(0);
            matrix_Y = result.GetLength(1);
            waiter = new WaitForSeconds(time);
        }
        private void Clean() {
            result = default;
            matrix_X = matrix_Y = 0;
            waiter = default;
            runner = null;
            CO_Generation = default;
        }

        protected override IEnumerator CO_Generate() {
            for (int i = 0; i < matrix_X; i++) {
                for (int j = 0; j < matrix_Y; j++) {
                    result[i, j].IsVisited = false;
                }

                yield return waiter;
            }

            int assigned = default;
            while (assigned < totalPlayers) {
                int i = Random.Range(0, matrix_X);
                int j = Random.Range(0, matrix_Y);
                TileCell selected = result[i, j];
                if (!selected.IsConfigured)
                    continue;

                if (selected.Behaviour == TileBehaviourTypes.Default) {
                    if(useLog) this.Log($"Spawning player at cell {selected.X},{selected.Y}");
                    GameObject player = Instantiate(playerPrefabs);
                    assigned++;
                }

                yield return waiter;
            }

            GenerationComplete();
        }
        public override void InjectRunner(IEnumeratorRunner<TileCell[,]> runner) {
            if (runner == null) {
                this.Log("Invalid Runner", LogType.Error);
                return;
            }

            this.runner = runner;
            OnGenerationStarted += runner.OnStart;
            OnGenerationCompleted += runner.OnComplete;
        }
        public override void StartGeneration() {
            Setup();
            CO_Generation = runner.Run(CO_Generate);
            OnGenerationStarted?.Invoke();
        }
        public override void GenerationComplete() {
            runner.Stop(CO_Generation);
            OnGenerationCompleted?.Invoke(result);
            Clean();
        }
    }
}