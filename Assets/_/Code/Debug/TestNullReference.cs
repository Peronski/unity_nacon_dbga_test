using NaconTest.RuntimePackages.Enums;
using UnityEngine;

public class TestNullReference : MonoBehaviour {
    [SerializeField] private DirectionTypes everything = default;

    private void OnValidate() {
        everything = (DirectionTypes)~0;
    }

    [ContextMenu(nameof(Everything_Equals_To_SumOf_AllDirections))]
    private void Everything_Equals_To_SumOf_AllDirections() {
        DirectionTypes test = DirectionTypes.Up | DirectionTypes.Down | DirectionTypes.Left | DirectionTypes.Right;

        this.MonoLog($"Everything equal to Sum: {test == everything}");
    }

    [ContextMenu(nameof(AllBits_EqualsTo_SumOf_AllDirections_Or_Everything))]
    private void AllBits_EqualsTo_SumOf_AllDirections_Or_Everything() {
        DirectionTypes test = DirectionTypes.Up | DirectionTypes.Down | DirectionTypes.Left | DirectionTypes.Right;
        DirectionTypes allBits = (DirectionTypes)~0;

        this.MonoLog($"Sum equal to allBits: {test == allBits}");
        this.MonoLog($"Everything equal to allBits: {everything == allBits}");
    }
}
