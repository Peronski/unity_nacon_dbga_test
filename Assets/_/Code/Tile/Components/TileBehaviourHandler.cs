using NaconTest.RuntimePackages.Enums;
using UnityEngine;

namespace NaconTest.RuntimePackages.Tile {
    /// <summary>
    /// This class should manage the special cells' behaviour (Monster, Acid, Teleport).
    /// Unfortunately I was unable to implement this part.
    /// 
    /// The starting ideas was to trigger the call of a specific behaviour action during player movement
    /// - OnEntering the cell (as fadingfog, UI feedbacks, ecc..) [while lerping through path points and reaching a certain zone]
    /// - OnStaying the cell (teleport, gameover, ecc..) [when reached final point of path]
    /// - OnExiting the cell [while lerping through path points and reaching a certain zone]
    /// </summary>
    public class TileBehaviourHandler : MonoBehaviour {
        // EXAMPLE
        // Dictionary<BehaviourTypes, LogicSO> behaviourLogics
        // BehaviourLogicSO currentLogic

        public void Configure(DirectionTypes directions, TileAspectTypes types, TileBehaviourTypes behaviours) {
            // currentLogic = behaviourLogics[behaviours];
        }

        public void OnEnter() {
            // currentLogic.OnEnter
        }
        public void OnStay() {
            // currentLogic.OnStay
        }
        public void OnExit() {
            // currentLogic.OnExit
        }
    }
}
