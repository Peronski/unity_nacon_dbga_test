using UnityEngine;
using UnityEngine.UI;

namespace NaconTest.RuntimePackages.Tile {
    public class TileVisualPathfinderHandler : MonoBehaviour {
        [Header("Settings")]
        [SerializeField] private Color debugColor = default;

        [Header("References")]
        [SerializeField] private Image visualDebug = default;

        public void ApplyColorDebugVisualization(bool visited) {
            visualDebug.color = visited ? debugColor : new Color(0, 0, 0, 0);
        }
    }
}
