using NaconTest.RuntimePackages.Enums;
using System;
using UnityEngine;

namespace NaconTest.RuntimePackages.Tile {
    /// <summary>
    /// This component was use as configuration container for debug purposes.
    /// </summary>
    [Obsolete]
    public class TileConfigurationHandler : MonoBehaviour {
        [Header("Settings")]
        [SerializeField] private DirectionTypes entrances = default;
        [SerializeField] private TileAspectTypes aspect = default;
        [SerializeField] private TileBehaviourTypes behaviour = default;
        [SerializeField] private bool hasFog = default;

        public DirectionTypes Entrances => entrances;
        public TileAspectTypes Aspect => aspect;
        public TileBehaviourTypes Behaviour => behaviour;

        public event Action<DirectionTypes, TileAspectTypes, TileBehaviourTypes, bool> OnConfigurationEvent = default;

        public void AssignEntrances(DirectionTypes entrances, bool or = true) {
            this.entrances = or ? this.entrances |= entrances : entrances ;
        }
        public void AssignAspect(TileAspectTypes aspect) {
            this.aspect = aspect;
        }
        public void AssignBehaviour(TileBehaviourTypes behaviour) {
            this.behaviour = behaviour;
        }
        public void AssignFog(bool hasFog) {
            this.hasFog = hasFog;
        }
        public void Configure() {
            OnConfigurationEvent?.Invoke(entrances, aspect, behaviour, hasFog);
        }
    }
}
