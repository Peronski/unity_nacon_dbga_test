using NaconTest.RuntimePackages.Enums;
using System.Collections.Generic;
using UnityEngine;

namespace NaconTest.RuntimePackages.Tile {
    public class TileUnityObject : MonoBehaviour {
        [Header("References")]
        [SerializeField] private TileSpriteHandler spriteHandler = default;
        [SerializeField] private TilePathHandler pathHandler = default;
        [SerializeField] private TileBehaviourHandler behaviourHandler = default;
        [SerializeField] private TileVisualPathfinderHandler pathfinderHandler = default;

        public void Configure(DirectionTypes direction, TileAspectTypes aspect, TileBehaviourTypes behaviour) {
            spriteHandler.Configure(direction, aspect, behaviour);
            pathHandler.Configure(direction, aspect, behaviour);
            behaviourHandler.Configure(direction, aspect, behaviour);
        }
        public void Visited(bool visited) {
            spriteHandler.FadeFog(visited);
        }
        public void DebugVisualization(bool enable) {
            pathfinderHandler.ApplyColorDebugVisualization(enable);
        }
        public Vector3 GetDefaultPoint() {
            return pathHandler.GetDefaultPoint();
        }
        public List<Vector3> GetPath() {
            return pathHandler.GetPath();
        }
    }
}
