using NaconTest.Data.Tile;
using NaconTest.RuntimePackages.Enums;
using UnityEngine;
using UnityEngine.UI;

namespace NaconTest.RuntimePackages.Tile {
    public class TileSpriteHandler : MonoBehaviour {
        [Header("References")]
        [SerializeField] private Image aspectAmbient = default;
        [SerializeField] private Image typeAmbient = default;
        [SerializeField] private Image fogAmbient = default;

        [Header("Data")]
        [SerializeField] private TileConfigurationContainer tileConfigurationContainer = default;

        public void Configure(DirectionTypes directions, TileAspectTypes types, TileBehaviourTypes behaviours) {
            Sprite aspect = tileConfigurationContainer.GetAspect(directions, types);
            Sprite type = tileConfigurationContainer.GetAspect(behaviours);
            Sprite fog = tileConfigurationContainer.GetFog();

            ApplySpriteDoors(aspect);
            ApplySpriteType(type);
            ApplySpriteFog(fog);
        }
        public void FadeFog(bool isVisited) {
            fogAmbient.color = isVisited ? tileConfigurationContainer.GetNoFogColor() : tileConfigurationContainer.GetFogColor();
        }

        private void ApplySpriteDoors(Sprite sprite) {
            if (sprite == null)
                return;

            aspectAmbient.sprite = sprite;
        }
        private void ApplySpriteType(Sprite sprite) {
            if (sprite == null)
                return;

            typeAmbient.sprite = sprite;
        }
        private void ApplySpriteFog(Sprite sprite) {
            if (sprite == null)
                return;

            fogAmbient.sprite = sprite;
        }
    }
}
