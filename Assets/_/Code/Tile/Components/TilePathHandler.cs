using Barbaragno.RuntimePackages.SerializableUtility.Dictionary;
using NaconTest.RuntimePackages.Enums;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace NaconTest.RuntimePackages.Tile {
    public class TilePathHandler : MonoBehaviour {
        [Header("Settings")]
        [SerializeField] private int tunnelPathPoint = default;

        [Header("References")]
        [SerializeField] private Transform defaultPoint = default;
        [SerializeField] private SerializableDictionary<DirectionTypes, Transform> directionPoints = new SerializableDictionary<DirectionTypes, Transform>();
        [SerializeField] private SerializableDictionary<DirectionTypes, Transform> anchorPoints = new SerializableDictionary<DirectionTypes, Transform>();

        private Transform anchorPoint = default;
        private List<Vector3> cellPath = default;


        public void Configure(DirectionTypes directions, TileAspectTypes types, TileBehaviourTypes behaviours) {
            if (types == TileAspectTypes.Default) {
                CreateDefaultPath();
                return;
            }

            if (!anchorPoints.ContainsKey(directions))
                return;

            anchorPoint = anchorPoints[directions];
            CreateTunnelPath(directions);
        }
        public Vector3 GetDefaultPoint() {
            return defaultPoint.position;
        }
        public List<Vector3> GetPath() {
            return cellPath;
        }
        public DirectionTypes GetOppositeExit(DirectionTypes entrances) {
            if (!directionPoints.ContainsKey(entrances))
                return DirectionTypes.None;

            return anchorPoints[anchorPoint] & ~entrances;
        }

        private void CreateDefaultPath() {
            cellPath = new List<Vector3>();
            cellPath.Add(defaultPoint.position);
        }
        private void CreateTunnelPath(DirectionTypes entrances) {
            cellPath = new List<Vector3>();
            DirectionTypes[] singleEntrances = entrances.GetFlags().ToArray();

            Transform startingPoint = directionPoints[singleEntrances[0]];
            Transform endingPoint = directionPoints[singleEntrances[1]];

            SetOrientationAnchorPoint(anchorPoint, startingPoint);

            for (int i = 0; i <= tunnelPathPoint; i++) {
                Vector3 position = AnchorRotationMovement(anchorPoint, endingPoint, i);
                cellPath.Add(position);
            }
        }

        private void SetOrientationAnchorPoint(Transform anchorPoint, Transform startingPoint) {
            Vector3 distance = GetDistanceFromTo(anchorPoint.position, startingPoint.position);
            anchorPoint.rotation = GetTargetAngleFromTo(anchorPoint, distance);
        }
        private Vector3 AnchorRotationMovement(Transform anchorPoint, Transform targetPoint, int part) {
            Vector3 distance = GetDistanceFromTo(anchorPoint.position, targetPoint.position);
            Quaternion targetRotation = GetTargetAngleFromTo(anchorPoint, distance);
            anchorPoint.rotation = Quaternion.Lerp(anchorPoint.rotation, targetRotation, (float)part / tunnelPathPoint);
            return anchorPoint.right;
        }
        private Vector3 GetDistanceFromTo(Vector3 anchorPoint, Vector3 targetPoint) {
            return targetPoint - anchorPoint;
        }
        private Quaternion GetTargetAngleFromTo(Transform anchorPoint, Vector3 distance) {
            Quaternion difference = Quaternion.FromToRotation(anchorPoint.right, distance.normalized);
            Quaternion targetRotation = anchorPoint.rotation * difference;
            return targetRotation;
        }
    }
}