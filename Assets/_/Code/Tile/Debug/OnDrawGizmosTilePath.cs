using NaconTest.RuntimePackages.Enums;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace NaconTest.Debug.Tile {
    public class OnDrawGizmosTilePath : MonoBehaviour {
#pragma warning disable
        [Header("OnDrawGizmos")]
        [SerializeField] private bool onDrawGizmos = default;
        [SerializeField] private bool orientation = default;
        [SerializeField] private bool rotation = default;
        [SerializeField] private Transform dummyTest = default;
        [SerializeField] private DirectionTypes entrance = default;
        [SerializeField] private DirectionTypes tileConfigDirection = default;

        [Header("Settings")]
        [SerializeField] private float speed = default;
        [Space]

        [Header("References")]
        [SerializeField] private Transform defaultPoint = default;
        [SerializeField] private List<PathDirectionPoint> directionPoints = new List<PathDirectionPoint>();
        [SerializeField] private List<PathAnchorPoint> anchorPoints = new List<PathAnchorPoint>();

        private Transform anchorPoint = default;
        private Transform startingPoint = default;
        private Transform endingPoint = default;
#pragma warning enable

        private void OnDrawGizmos() {
            if (!onDrawGizmos)
                return;

            DirectionTypes endingDirection = tileConfigDirection & ~entrance;
            anchorPoint = anchorPoints.FirstOrDefault(point => point.direction == tileConfigDirection).point;
            startingPoint = directionPoints.FirstOrDefault(point => point.direction == entrance).point;
            endingPoint = directionPoints.FirstOrDefault(point => point.direction == endingDirection).point;

            this.MonoLog($"Starting point: {startingPoint}");
            this.MonoLog($"Ending point: {endingPoint}");
            this.MonoLog($"Anchor point: {anchorPoint}");

            onDrawGizmos = false;

            //if (orientation) {
            //    SetOrientationAnchorPoint(anchorPoint, startingPoint);
            //    orientation = false;
            //}

            //if (rotation) {
            //    AnchorRotationMovement(anchorPoint, endingPoint);
            //}
        }

        private void SetOrientationAnchorPoint(Transform anchorPoint, Transform startingPoint) {
            Vector3 distance = GetDistanceFromTo(anchorPoint.position, startingPoint.position);
            anchorPoint.rotation = GetTargetAngleFromTo(anchorPoint, distance);
        }
        private void AnchorRotationMovement(Transform anchorPoint, Transform targetPoint) {
            Vector3 distance = GetDistanceFromTo(anchorPoint.position, targetPoint.position);
            Quaternion targetRotation = GetTargetAngleFromTo(anchorPoint, distance);
            anchorPoint.rotation = Quaternion.Lerp(anchorPoint.rotation, targetRotation, speed * Time.deltaTime);
        }

        private Vector3 GetDistanceFromTo(Vector3 anchorPoint, Vector3 targetPoint) {
            return targetPoint - anchorPoint;
        }
        private Quaternion GetTargetAngleFromTo(Transform anchorPoint, Vector3 distance) {
            Quaternion difference = Quaternion.FromToRotation(anchorPoint.right, distance.normalized);
            Quaternion targetRotation = anchorPoint.rotation * difference;
            return targetRotation;
        }
    }

    [System.Serializable]
    public class PathDirectionPoint {
        [field: SerializeField] public DirectionTypes direction { get; private set; }
        [field: SerializeField] public Transform point { get; private set; }
    }
    [System.Serializable]
    public class PathAnchorPoint {
        [field: SerializeField] public DirectionTypes direction { get; private set; }
        [field: SerializeField] public Transform point { get; private set; }
    }
}