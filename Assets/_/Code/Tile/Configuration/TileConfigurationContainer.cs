using NaconTest.RuntimePackages.Enums;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace NaconTest.Data.Tile {
    [CreateAssetMenu(fileName = "New Tile Configurations Container", menuName = "ScriptableObjects/Tile Configurations Container")]
    [System.Serializable]
    public class TileConfigurationContainer : ScriptableObject {
        [Header("Settings")]
        [SerializeField] private Color defaultFog = default;
        [SerializeField] private Color noFog = default;

        [Header("References")]
        [SerializeField] private Sprite fog = default;
        [Space]
        [SerializeField] private List<TileSpriteDirection> defaultSprites = new List<TileSpriteDirection>();
        [SerializeField] private List<TileSpriteDirection> tunnelSprites = new List<TileSpriteDirection>();
        [SerializeField] private List<TileSpriteType> behaviourSprites = new List<TileSpriteType>();

        public Sprite GetAspect(DirectionTypes direction, TileAspectTypes aspect) {
            if (aspect == TileAspectTypes.Default) {
                if(direction.GetNumberFlags() >= 4) 
                    direction = (DirectionTypes)~0;

                return defaultSprites.FirstOrDefault(tile => tile.doors == direction).aspect;
            }

            if (aspect == TileAspectTypes.Tunnel)
                return tunnelSprites.FirstOrDefault(tile => tile.doors == direction).aspect;

            this.Log("Invalid configuration parameters.");
            return null;
        }
        public Sprite GetAspect(TileBehaviourTypes behaviour) {
            return behaviourSprites.FirstOrDefault(tile => tile.type == behaviour).aspect;
        }
        public Sprite GetFog() {
            return fog;
        }
        public Color GetFogColor() {
            return defaultFog;
        }
        public Color GetNoFogColor() {
            return noFog;
        }
    }

    [System.Serializable]
    public class TileSpriteDirection {
        [field: SerializeField] public DirectionTypes doors { get; private set; }
        [field: SerializeField] public Sprite aspect { get; private set; }
    }
    [System.Serializable]
    public class TileSpriteType {
        [field: SerializeField] public TileBehaviourTypes type { get; private set; }
        [field: SerializeField] public Sprite aspect { get; private set; }
    }
}