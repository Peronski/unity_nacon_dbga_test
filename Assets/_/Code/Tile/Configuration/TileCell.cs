using NaconTest.RuntimePackages.Enums;

namespace NaconTest.RuntimePackages.Tile {
    public class TileCell {
        public TileCell(int x, int y, TileUnityObject unityObject) {
            X = x;
            Y = y;
            TileUnityObject = unityObject;
        }

        private bool pathfinderHasVisited = default;
        private bool isConfigured = default;

        private bool isVisited = default;

        private DirectionTypes directionType = default;
        private TileAspectTypes aspectType = default;
        private TileBehaviourTypes behaviourType = default;

        public int X { get; private set; }
        public int Y { get; private set; }

        public bool IsVisited {
            get {
                return isVisited;
            }
            set {
                isVisited = value;
                BecameVisited(value);
            }
        }

        public bool IsConfigured {
            get {
                return isConfigured;
            }
            set {
                isConfigured = value;
                if (isConfigured) LaunchConfiguration();
            }
        }

        // This variable should not be here -> Simplicity and debug purposes
        public bool PathfinderHasVisited {
            get { 
                return pathfinderHasVisited; 
            }
            set {
                pathfinderHasVisited = value;
                DebugVisualization(value);
            }
        }

        public DirectionTypes Entrances => directionType;
        public TileBehaviourTypes Behaviour => behaviourType;

        public TileUnityObject TileUnityObject { get; private set; }

        public void SetDirections(DirectionTypes directionTypes, bool OR = true) {
            directionType  = OR ? directionType | directionTypes : directionTypes;
        }
        public void SetAspect(TileAspectTypes aspect) {
            aspectType = aspect;
        }
        public void SetBehaviour(TileBehaviourTypes behaviour) {
            behaviourType = behaviour;
        }

        private void LaunchConfiguration() {
            TileUnityObject.Configure(directionType, aspectType, behaviourType);
        }
        private void BecameVisited(bool visited) {
            TileUnityObject.Visited(visited);
        }
        private void DebugVisualization(bool visualize) {
            TileUnityObject.DebugVisualization(visualize);
        }
    }
}